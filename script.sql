drop database if exists brookeDB;
create database brookeDB;
use brookeDB;

create table utilisateur(
id int primary key auto_increment,
courriel varchar(50),
nom varchar(30),
prenom varchar(30),
naissance timestamp,
telephone int(10),
pssw varchar(20),
adresse varchar(100),
ville varchar(30),
province varchar(30),
pays varchar(30),
cp varchar(7),
adresseLivraison varchar(100),
villeLivraison varchar(30),
provinceLivraison varchar(30),
paysLivraison varchar(30),
cpLivraison varchar(7),
role varchar(14));

create table categorie(
id int primary key auto_increment,
nom varchar(50),
description varchar(255));

create table catalogue(
idLivre int primary key auto_increment,
nom varchar(50),
description varchar(255),
image varchar(255),
categorieId int,
foreign key (categorieId) references categorie(id));

SELECT * from utilisateur;
/*insert into utilisateur values (null, null, null, null, '2012-08-01', null, null, null, null, null,null, null, null, null,null, null, null, null);*/