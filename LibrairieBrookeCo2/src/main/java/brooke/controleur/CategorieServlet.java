package brooke.controleur;

import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


import javax.sql.DataSource;

import brooke.modele.Categorie;
import brooke.service.CategorieDBService;
import brooke.service.InscriptionDBService;


public class CategorieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/brookeDB")
	private DataSource dataSource;
	
	CategorieDBService categorieDBService;

    public CategorieServlet() {
        super();
        
    }
    public void init() throws ServletException{
    	super.init();
        try {
        	categorieDBService = new CategorieDBService(dataSource);
        }catch(Exception exc) {
        	throw new ServletException(exc);
        }
    	
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/nouvelleCategorie.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		switch(request.getParameter("action")) {
			case "ajout" : 
				String nom = request.getParameter("nom");
				String description= request.getParameter("description");
				try {
					categorieDBService.ajouterCategorie(nom, description);
					RequestDispatcher dispatcher = request.getRequestDispatcher("/nouvelleCategorie.jsp");
					dispatcher.forward(request, response);

				} catch (Exception e) {
					// AFFICHER PAGE D'ERREUR
					e.printStackTrace();
				}
				break;
			case "liste" :
				try {
					List<Categorie> categories = categorieDBService.listeCategories();
					request.setAttribute("categories", categories);
					
					
				} catch (Exception e) {
					// AFFICHER PAGE D'ERREUR
					e.printStackTrace();
				}
		}
		

		
	}


}
