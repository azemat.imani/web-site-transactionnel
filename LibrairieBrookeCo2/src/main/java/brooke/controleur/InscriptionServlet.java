package brooke.controleur;

import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

import javax.sql.DataSource;

import brooke.modele.Utilisateur;
import brooke.service.InscriptionDBService;

/**
 * Servlet implementation class InscriptionServlet
 */
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private InscriptionDBService inscriptionDBService;
	
	@Resource(name="jdbc/brookeDB")
	private DataSource dataSource;
		
	
    public InscriptionServlet() {
        super();

    }
    public void init() throws ServletException{
    	super.init();
        try {
        	inscriptionDBService = new InscriptionDBService(dataSource);
        }catch(Exception exc) {
        	throw new ServletException(exc);
        }
    	
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String courriel = request.getParameter("courriel");
		String nom = request.getParameter("nom");
		String pssw = request.getParameter("pssw");
		Utilisateur utilisateur = new Utilisateur(courriel);
		utilisateur.setNom(nom);
		utilisateur.setPssw(pssw);
		try {
			inscriptionDBService.inscrireUtilisateur(utilisateur);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/Accueil.jsp");
			dispatcher.forward(request, response);
		} catch (Exception e) {
			//AFFICHER PAGE D'ERREUR
			e.printStackTrace();
		}
		
	}

}
