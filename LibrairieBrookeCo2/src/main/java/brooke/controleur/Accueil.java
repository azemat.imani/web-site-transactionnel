package brooke.controleur;

import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;
import java.util.List;

import javax.sql.DataSource;

import brooke.modele.Categorie;
import brooke.modele.Utilisateur;
import brooke.service.CategorieDBService;
import brooke.service.UtilisateurDBService;


public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(name="jdbc/brookeDB")
	private DataSource dataSource;
	private Utilisateur utilisateur;
	private List<Categorie> categories;
	
	UtilisateurDBService utilisateurDBService;
	CategorieDBService categorieDBService;
	
	
    public Accueil() {
        super();
        
    }

	public void init(ServletConfig config) throws ServletException {
		try {
        	categorieDBService = new CategorieDBService(dataSource);
        	categories=categorieDBService.listeCategories();
        }catch(Exception exc) {
        	throw new ServletException(exc);
        }
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setAttribute("listeCategories", categories);
		
		String action = request.getParameter("action");
		RequestDispatcher dispatcher;
		
		if(action==null) {
			dispatcher = request.getRequestDispatcher("/Accueil.jsp");
			dispatcher.forward(request, response);
		}else {
			switch(action) {
			case "CONNECTER":
				dispatcher = request.getRequestDispatcher("/seConnecter.jsp");
				dispatcher.forward(request, response);
				break;
			case "CREER":
				dispatcher = request.getRequestDispatcher("/Inscription.jsp");
				dispatcher.forward(request, response);
				break;
			}
		}

	}
	//traite le login
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		HttpSession session = request.getSession();
//		utilisateurDBService = new UtilisateurDBService(dataSource);
//		
//		try {
//			utilisateur = utilisateurDBService.login(request.getParameter("courriel"), request.getParameter("pssw"));
//			RequestDispatcher dispatcher;			
//			if(utilisateur!=null){
//				session.setAttribute("utilisateur", utilisateur);
//				dispatcher=request.getRequestDispatcher("/Accueil");
//				dispatcher.forward(request, response);
//			}else {
//				request.setAttribute("StringError", "Informations invalides");
//				//dispatcher = re
//			}
//		} catch (Exception e) {
//			// PAGE ERREUR
//			e.printStackTrace();
//		}
		
	}

}
