package brooke.modele;

import java.sql.Timestamp;

public class Utilisateur {
	public int id;
	public String courriel;
	public String nom;
	public String prenom;
	public Timestamp naissance;
	public int telephone;
	public String pssw;
	public String adresse;
	public String ville;
	public String province;
	public String pays;
	public String cp;
	public String adresseLivraison;
	public String villeLivraison;
	public String provinceLivraison;
	public String paysLivraison;
	public String cpLivraison;
	public String role;
	
	public Utilisateur(String courriel) {
		this.courriel=courriel;
	}
	public Utilisateur(int id, String courriel, String nom, String prenom, Timestamp naissance, int telephone,
			String pssw, String adresse, String ville, String province, String pays, String cp, String adresseLivraison,
			String villeLivraison, String provinceLivraison, String paysLivraison, String cpLivraison) {
		super();
		this.id = id;
		this.courriel = courriel;
		this.nom = nom;
		this.prenom = prenom;
		this.naissance = naissance;
		this.telephone = telephone;
		this.pssw = pssw;
		this.adresse = adresse;
		this.ville = ville;
		this.province = province;
		this.pays = pays;
		this.cp = cp;
		this.adresseLivraison = adresseLivraison;
		this.villeLivraison = villeLivraison;
		this.provinceLivraison = provinceLivraison;
		this.paysLivraison = paysLivraison;
		this.cpLivraison = cpLivraison;

	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCourriel() {
		return courriel;
	}
	public void setCourriel(String courriel) {
		this.courriel = courriel;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Timestamp getNaissance() {
		return naissance;
	}
	public void setNaissance(Timestamp naissance) {
		this.naissance = naissance;
	}
	public int getTelephone() {
		return telephone;
	}
	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	public String getPssw() {
		return pssw;
	}
	public void setPssw(String pssw) {
		this.pssw = pssw;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getAdresseLivraison() {
		return adresseLivraison;
	}
	public void setAdresseLivraison(String adresseLivraison) {
		this.adresseLivraison = adresseLivraison;
	}
	public String getVilleLivraison() {
		return villeLivraison;
	}
	public void setVilleLivraison(String villeLivraison) {
		this.villeLivraison = villeLivraison;
	}
	public String getProvinceLivraison() {
		return provinceLivraison;
	}
	public void setProvinceLivraison(String provinceLivraison) {
		this.provinceLivraison = provinceLivraison;
	}
	public String getPaysLivraison() {
		return paysLivraison;
	}
	public void setPaysLivraison(String paysLivraison) {
		this.paysLivraison = paysLivraison;
	}
	public String getCpLivraison() {
		return cpLivraison;
	}
	public void setCpLivraison(String cpLivraison) {
		this.cpLivraison = cpLivraison;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	

}
