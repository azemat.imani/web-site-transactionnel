package brooke.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import brooke.modele.Utilisateur;

public class UtilisateurDBService {
	private DataSource dataSource;

	public UtilisateurDBService(DataSource dataSource) {
		super();
		this.dataSource = dataSource;
	}
	
	@SuppressWarnings("null")
	public Utilisateur login(String courriel, String pssw) throws Exception {
		Utilisateur utilisateur=null;
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			connection = dataSource.getConnection();
			
			String sql = "SELECT * FROM utilisateur WHERE courriel = '"+courriel+"' LIMIT 1;";
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(sql);
			
			if(resultSet.next()) {
				if(resultSet.getString("pssw")==pssw) {
					utilisateur.setId(resultSet.getInt("id"));
					utilisateur.setCourriel(resultSet.getString("courriel"));
					utilisateur.setNom(resultSet.getString("nom"));
					utilisateur.setPrenom(resultSet.getString("prenom"));
					utilisateur.setNaissance(resultSet.getTimestamp("naissance"));
					utilisateur.setTelephone(resultSet.getInt("telephone"));
					utilisateur.setPssw(resultSet.getString("pssw"));
					utilisateur.setAdresse(resultSet.getString("adresse"));
					utilisateur.setVille(resultSet.getString("ville"));
					utilisateur.setProvince(resultSet.getString("province"));
					utilisateur.setPays(resultSet.getString("pays"));
					utilisateur.setCp(resultSet.getString("cp"));
					utilisateur.setAdresseLivraison(resultSet.getString("adresseLivraison"));
					utilisateur.setVilleLivraison(resultSet.getString("villeLivraison"));
					utilisateur.setProvinceLivraison(resultSet.getString("provinceLivraison"));
					utilisateur.setPaysLivraison(resultSet.getString("paysLivraison"));
					utilisateur.setCpLivraison(resultSet.getString("cpLivraison"));
					utilisateur.setRole(resultSet.getString("role"));
			
					
				}

			}
			return utilisateur;
			
		}finally {
			close(connection, statement, resultSet);
		}
				
	}
	
	private void close(Connection connection, Statement statement, ResultSet resultSet) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}
			
			if (statement != null) {
				statement.close();
			}
			
			if (connection != null) {
				connection.close();
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

}
