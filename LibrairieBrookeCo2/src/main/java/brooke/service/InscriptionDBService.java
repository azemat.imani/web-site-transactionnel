package brooke.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.sql.DataSource;

import brooke.modele.Utilisateur;

public class InscriptionDBService {
	private DataSource dataSource;
	
	public InscriptionDBService(DataSource ds) {
		this.dataSource=ds;
	}
	
	public void inscrireUtilisateur(Utilisateur utilisateur) throws Exception {
		Connection connection = null;
		Statement statement = null;
		
		
		try {
			connection = dataSource.getConnection();
			//id, courriel, nom, prenom, naissance, telephone, pssw, adresse, ville, 
			//province, pays, cp, adresseLivraison, villeLivraison, provinceLivraison, paysLivraison, cpLivraison, role
			String sql = "INSERT INTO utilisateur VALUES ("
					+ null+","
					+ "\'"+ utilisateur.getCourriel()+"\',"	
					+ "\'"+ utilisateur.getNom() +"\',"
					+ "\'"+ utilisateur.getPrenom() +"\',"
					+ utilisateur.getNaissance() +","
					+ utilisateur.getTelephone() + ","
					+ "\'"+ utilisateur.getPssw() +"\',"
					+ "\'"+ utilisateur.getAdresse() +"\',"
					+ "\'"+ utilisateur.getVille() +"\',"
					+ "\'"+ utilisateur.getProvince() +"\',"
					+ "\'"+ utilisateur.getPays() +"\',"
					+ "\'"+ utilisateur.getCp() +"\',"
					+ "\'"+ utilisateur.getAdresseLivraison() +"\',"
					+ "\'"+ utilisateur.getVilleLivraison() +"\',"
					+ "\'"+ utilisateur.getProvinceLivraison() +"\',"
					+ "\'"+ utilisateur.getPaysLivraison() +"\',"
					+ "\'"+ utilisateur.getCpLivraison() +"\',"
					+ "'client');";
			statement = connection.createStatement();
			statement.execute(sql);
			
		}finally {
			close(connection, statement);
		}
	}
	private void close(Connection connection, Statement statement) {

		try {
			
			if (statement != null) {
				statement.close();
			}
			
			if (connection != null) {
				connection.close();
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

}

