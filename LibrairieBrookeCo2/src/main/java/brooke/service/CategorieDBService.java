package brooke.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import brooke.modele.Categorie;

public class CategorieDBService {
	private DataSource dataSource;

	public CategorieDBService(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public void ajouterCategorie(String nom, String description) throws Exception {
		Connection connection = null;
		Statement statement = null;
		
		try {
			connection = dataSource.getConnection();
			String sql="INSERT INTO categorie VALUES (null, \'"+nom+"\',\'"+description+"\');";
			statement = connection.createStatement();
			statement.execute(sql);
		}finally {
			close(connection,statement);
		}
	}
	
	public List<Categorie> listeCategories() throws Exception{
		List<Categorie> listeCategories=new ArrayList<>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			connection = dataSource.getConnection();
			
			String sql = "select * from categorie order by nom";
			
			statement = connection.createStatement();
			
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				
				int id = resultSet.getInt("id");
				String nom = resultSet.getString("nom");
				String description = resultSet.getString("description");
				
				Categorie catTemp= new Categorie(id, nom, description);
				
				listeCategories.add(catTemp);				
			}
			
			return listeCategories;		
		}
		finally {
			close(connection, statement, resultSet);
		}
		
	}
	private void close(Connection connection, Statement statement) {

		try {
			
			if (statement != null) {
				statement.close();
			}
			
			if (connection != null) {
				connection.close();
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
	private void close(Connection connection, Statement statement, ResultSet resultSet) {

		try {
			if (resultSet != null) {
				resultSet.close();
			}
			
			if (statement != null) {
				statement.close();
			}
			
			if (connection != null) {
				connection.close();
			}
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}

}
