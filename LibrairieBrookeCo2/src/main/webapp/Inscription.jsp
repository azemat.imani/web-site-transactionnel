<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="css/style.css">
<title>Création de compte</title>
</head>
<body>
<%@ include file = "header.jsp" %>
<%@ include file = "inscriptionForm.jsp" %>
</body>
</html>