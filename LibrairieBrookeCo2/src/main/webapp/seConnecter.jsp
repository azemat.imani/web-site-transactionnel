<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" session="false"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/style.css">
<meta charset="utf-8">
<title>Se connecter</title>
</head>
<body>
<%@ include file = "header.jsp" %>
<form name="loginForm" method="POST" action="j_security_check">
    <table>
        <tr>
            <td>Courriel: </td>
            <td><input type="text" name="j_username"  size="40"></td>
        </tr>
        <tr>
            <td>Mot de passe: </td>
            <td><input type="text" name="j_password" size="40"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Se connecter">
            	<a href="Accueil?action=CREER">
            		<input type="button" value="Créer un compte">
            	</a>
                
            </td>
        </tr>
    </table>
</form>
</body>
</html>